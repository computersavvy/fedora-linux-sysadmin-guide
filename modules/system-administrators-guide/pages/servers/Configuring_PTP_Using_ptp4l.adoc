
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Configuring_PTP_Using_ptp4l]]
= Configuring PTP Using ptp4l

[[sec-Introduction_to_PTP]]
== Introduction to PTP

The _Precision Time Protocol_ (*PTP*) is a protocol used to synchronize clocks in a network. When used in conjunction with hardware support, `PTP` is capable of sub-microsecond accuracy, which is far better than is normally obtainable with `NTP`. `PTP` support is divided between the kernel and user space. The kernel in Fedora includes support for `PTP` clocks, which are provided by network drivers. The actual implementation of the protocol is known as [application]*linuxptp*, a `PTPv2` implementation according to the IEEE standard 1588 for Linux.

The [package]*linuxptp* package includes the [application]*ptp4l* and [application]*phc2sys* programs for clock synchronization. The [application]*ptp4l* program implements the `PTP` boundary clock and ordinary clock. With hardware time stamping, it is used to synchronize the `PTP` hardware clock to the master clock, and with software time stamping it synchronizes the system clock to the master clock. The [application]*phc2sys* program is needed only with hardware time stamping, for synchronizing the system clock to the `PTP` hardware clock on the _network interface card_ (*NIC*).

[[sec-Understanding_PTP]]
=== Understanding PTP

The clocks synchronized by `PTP` are organized in a master-slave hierarchy. The slaves are synchronized to their masters which may be slaves to their own masters. The hierarchy is created and updated automatically by the _best master clock_ (*BMC*) algorithm, which runs on every clock. When a clock has only one port, it can be _master_ or _slave_, such a clock is called an _ordinary clock_ (*OC*). A clock with multiple ports can be master on one port and slave on another, such a clock is called a _boundary_ clock (*BC*). The top-level master is called the _grandmaster clock_, which can be synchronized by using a _Global Positioning System_ (*GPS*) time source. By using a GPS-based time source, disparate networks can be synchronized with a high-degree of accuracy.

[[exam-Understanding_PTP]]
.PTP grandmaster, boundary, and slave Clocks

image::ptp_grandmaster_boundary_and_slaves.png[An illustration showing PTP grandmaster, boundary, and slave clocks]

[[sec-Advantages_of_PTP]]
=== Advantages of PTP

One of the main advantages that `PTP` has over the _Network Time Protocol_ (*NTP*) is hardware support present in various _network interface controllers_ (*NIC*) and network switches. This specialized hardware allows `PTP` to account for delays in message transfer, and greatly improves the accuracy of time synchronization. While it is possible to use non-PTP enabled hardware components within the network, this will often cause an increase in jitter or introduce an asymmetry in the delay resulting in synchronization inaccuracies, which add up with multiple non-PTP aware components used in the communication path. To achieve the best possible accuracy, it is recommended that all networking components between `PTP` clocks are `PTP` hardware enabled. Time synchronization in larger networks where not all of the networking hardware supports `PTP` might be better suited for `NTP`.

With hardware `PTP` support, the NIC has its own on-board clock, which is used to time stamp the received and transmitted `PTP` messages. It is this on-board clock that is synchronized to the `PTP` master, and the computer's system clock is synchronized to the `PTP` hardware clock on the NIC. With software `PTP` support, the system clock is used to time stamp the `PTP` messages and it is synchronized to the `PTP` master directly. Hardware `PTP` support provides better accuracy since the NIC can time stamp the `PTP` packets at the exact moment they are sent and received while software `PTP` support requires additional processing of the `PTP` packets by the operating system.

[[sec-Using_PTP]]
== Using PTP

In order to use `PTP`, the kernel network driver for the intended interface has to support either software or hardware time stamping capabilities.

[[sec-Checking_for_Driver_and_Hardware_Support]]
=== Checking for Driver and Hardware Support

In addition to hardware time stamping support being present in the driver, the NIC must also be capable of supporting this functionality in the physical hardware. The best way to verify the time stamping capabilities of a particular driver and NIC is to use the [application]*ethtool* utility to query the interface as follows:

[subs="attributes"]
----
~]#{nbsp}ethtool -T em3
Time stamping parameters for em3:
Capabilities:
        hardware-transmit     (SOF_TIMESTAMPING_TX_HARDWARE)
        software-transmit     (SOF_TIMESTAMPING_TX_SOFTWARE)
        hardware-receive      (SOF_TIMESTAMPING_RX_HARDWARE)
        software-receive      (SOF_TIMESTAMPING_RX_SOFTWARE)
        software-system-clock (SOF_TIMESTAMPING_SOFTWARE)
        hardware-raw-clock    (SOF_TIMESTAMPING_RAW_HARDWARE)
PTP Hardware Clock: 0
Hardware Transmit Timestamp Modes:
        off                   (HWTSTAMP_TX_OFF)
        on                    (HWTSTAMP_TX_ON)
Hardware Receive Filter Modes:
        none                  (HWTSTAMP_FILTER_NONE)
        all                   (HWTSTAMP_FILTER_ALL)
----

Where _em3_ is the interface you want to check.

For software time stamping support, the parameters list
should include:

* `SOF_TIMESTAMPING_SOFTWARE`

* `SOF_TIMESTAMPING_TX_SOFTWARE`

* `SOF_TIMESTAMPING_RX_SOFTWARE`

For hardware time stamping support, the parameters list
should include:

* `SOF_TIMESTAMPING_RAW_HARDWARE`

* `SOF_TIMESTAMPING_TX_HARDWARE`

* `SOF_TIMESTAMPING_RX_HARDWARE`

[[sec-Installing_PTP]]
=== Installing PTP

The kernel in Fedora includes support for `PTP`. User space support is provided by the tools in the [application]*linuxptp* package. To install [application]*linuxptp*, issue the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}dnf install linuxptp
----

This will install [application]*ptp4l* and [application]*phc2sys*.

Do not run more than one service to set the system clock's time at the same time. If you intend to serve `PTP` time using `NTP`, see xref:Configuring_PTP_Using_ptp4l.adoc#sec-Serving_PTP_Time_with_NTP[Serving PTP Time with NTP].

[[sec-Starting_ptp4l]]
=== Starting ptp4l

The [application]*ptp4l* program can be started from the command line or it can be started as a service. When running as a service, options are specified in the `/etc/sysconfig/ptp4l` file. Options required for use both by the service and on the command line should be specified in the `/etc/ptp4l.conf` file. The `/etc/sysconfig/ptp4l` file includes the [command]#-f /etc/ptp4l.conf# command line option, which causes the `ptp4l` program to read the `/etc/ptp4l.conf` file and process the options it contains. The use of the `/etc/ptp4l.conf` is explained in xref:Configuring_PTP_Using_ptp4l.adoc#sec-Specifying_a_Configuration_File[Specifying a Configuration File]. More information on the different [application]*ptp4l* options and the configuration file settings can be found in the `ptp4l(8)` man page.

.Starting ptp4l as a Service
To start [application]*ptp4l* as a service, issue the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}systemctl start ptp4l
----

.Using ptp4l From The Command Line
The [application]*ptp4l* program tries to use hardware time stamping by default. To use [application]*ptp4l* with hardware time stamping capable drivers and NICs, you must provide the network interface to use with the [option]`-i` option. Enter the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}ptp4l -i em3 -m
----

Where _em3_ is the interface you want to configure. Below is example output from [application]*ptp4l* when the `PTP` clock on the NIC is synchronized to a master:

[subs="attributes"]
----
~]#{nbsp}ptp4l -i em3 -m
selected em3 as PTP clock
port 1: INITIALIZING to LISTENING on INITIALIZE
port 0: INITIALIZING to LISTENING on INITIALIZE
port 1: new foreign master 00a069.fffe.0b552d-1
selected best master clock 00a069.fffe.0b552d
port 1: LISTENING to UNCALIBRATED on RS_SLAVE
master offset -23947 s0 freq +0 path delay       11350
master offset -28867 s0 freq +0 path delay       11236
master offset -32801 s0 freq +0 path delay       10841
master offset -37203 s1 freq +0 path delay       10583
master offset  -7275 s2 freq -30575 path delay   10583
port 1: UNCALIBRATED to SLAVE on MASTER_CLOCK_SELECTED
master offset  -4552 s2 freq -30035 path delay   10385

----

The master offset value is the measured offset from the master in nanoseconds. The `s0`, `s1`, `s2` strings indicate the different clock servo states: `s0` is unlocked, `s1` is clock step and `s2` is locked. Once the servo is in the locked state (`s2`), the clock will not be stepped (only slowly adjusted) unless the [option]`pi_offset_const` option is set to a positive value in the configuration file (described in the `ptp4l(8)` man page). The `adj` value is the frequency adjustment of the clock in parts per billion (ppb). The path delay value is the estimated delay of the synchronization messages sent from the master in nanoseconds. Port 0 is a Unix domain socket used for local `PTP` management. Port 1 is the `em3` interface (based on the example above.) INITIALIZING, LISTENING, UNCALIBRATED and SLAVE are some of possible port states which change on the INITIALIZE, RS_SLAVE, MASTER_CLOCK_SELECTED events. In the last state change message, the port state changed from UNCALIBRATED to SLAVE indicating successful synchronization with a `PTP` master clock.

The [application]*ptp4l* program can also be started as a service by running:

[subs="attributes"]
----
~]#{nbsp}systemctl start ptp4l
----

When running as a service, options are specified in the `/etc/sysconfig/ptp4l` file. More information on the different [application]*ptp4l* options and the configuration file settings can be found in the `ptp4l(8)` man page.

By default, messages are sent to `/var/log/messages`. However, specifying the [option]`-m` option enables logging to standard output which can be useful for debugging purposes.

To enable software time stamping, the [option]`-S` option needs to be used as follows:

[subs="attributes"]
----
~]#{nbsp}ptp4l -i em3 -m -S
----

[[sec-Selecting_a_Delay_Mechanism]]
==== Selecting a Delay Measurement Mechanism

There are two different delay measurement mechanisms and they can be selected by means of an option added to the [command]#ptp4l# command as follows:

[option]`-P`:: The [option]`-P` selects the _peer-to-peer_ (*P2P*) delay measurement mechanism.

The *P2P* mechanism is preferred as it reacts to changes in the network topology faster, and may be more accurate in measuring the delay, than other mechanisms. The *P2P* mechanism can only be used in topologies where each port exchanges *PTP* messages with at most one other *P2P* port. It must be supported and used by all hardware, including transparent clocks, on the communication path.

[option]`-E`:: The [option]`-E` selects the _end-to-end_ (*E2E*) delay measurement mechanism. This is the default.

The *E2E* mechanism is also referred to as the delay "`request-response`" mechanism.

[option]`-A`:: The [option]`-A` enables automatic selection of the delay measurement mechanism.

The automatic option starts [application]*ptp4l* in *E2E* mode. It will change to *P2P* mode if a peer delay request is received.

[NOTE]
====

All clocks on a single `PTP` communication path must use the same mechanism to measure the delay. Warnings will be printed in the following circumstances:

* When a peer delay request is received on a port using the *E2E* mechanism.

* When a *E2E* delay request is received on a port using the *P2P* mechanism.

====

[[sec-Specifying_a_Configuration_File]]
== Specifying a Configuration File

The command line options and other options, which cannot be set on the command line, can be set in an optional configuration file.

No configuration file is read by default, so it needs to be specified at runtime with the [option]`-f` option. For example:

[subs="attributes"]
----
~]#{nbsp}ptp4l -f /etc/ptp4l.conf
----

A configuration file equivalent to the [command]#-i em3 -m -S# options shown above would look as follows:

[subs="attributes"]
----
~]#{nbsp}cat /etc/ptp4l.conf
[global]
verbose               1
time_stamping         software
[em3]

----

[[sec-Using_the_PTP_Management_Client]]
== Using the PTP Management Client

The `PTP` management client, [application]*pmc*, can be used to obtain additional information from [application]*ptp4l* as follows:

[subs="attributes"]
----
~]#{nbsp}pmc -u -b 0 'GET CURRENT_DATA_SET'
sending: GET CURRENT_DATA_SET
        90e2ba.fffe.20c7f8-0 seq 0 RESPONSE MANAGMENT CURRENT_DATA_SET
                stepsRemoved        1
                offsetFromMaster  -142.0
                meanPathDelay     9310.0
----

[subs="attributes"]
----
~]#{nbsp}pmc -u -b 0 'GET TIME_STATUS_NP'
sending: GET TIME_STATUS_NP
        90e2ba.fffe.20c7f8-0 seq 0 RESPONSE MANAGMENT TIME_STATUS_NP
                master_offset              310
                ingress_time               1361545089345029441
                cumulativeScaledRateOffset   +1.000000000
                scaledLastGmPhaseChange    0
                gmTimeBaseIndicator        0
                lastGmPhaseChange          0x0000'0000000000000000.0000
                gmPresent                  true
                gmIdentity                 00a069.fffe.0b552d
----

Setting the [option]`-b` option to `zero` limits the boundary to the locally running [application]*ptp4l* instance. A larger boundary value will retrieve the information also from `PTP` nodes further from the local clock. The retrievable information includes:

* `stepsRemoved` is the number of communication paths to the grandmaster clock.

* `offsetFromMaster` and master_offset is the last measured offset of the clock from the master in nanoseconds.

* `meanPathDelay` is the estimated delay of the synchronization messages sent from the master in nanoseconds.

* if `gmPresent` is true, the `PTP` clock is synchronized to a master, the local clock is not the grandmaster clock.

* `gmIdentity` is the grandmaster's identity.

For a full list of [application]*pmc* commands, type the following as `root`:

[subs="attributes"]
----
~]#{nbsp}pmc help
----

Additional information is available in the `pmc(8)` man page.

[[sec-Synchronizing_the_Clocks]]
== Synchronizing the Clocks

The [application]*phc2sys* program is used to synchronize the system clock to the `PTP` hardware clock (*PHC*) on the NIC. The [application]*phc2sys* service is configured in the `/etc/sysconfig/phc2sys` configuration file. The default setting in the `/etc/sysconfig/phc2sys` file is as follows:

[subs="quotes"]
----
OPTIONS="-a -r"
----

The [option]`-a` option causes [application]*phc2sys* to read the clocks to be synchronized from the [application]*ptp4l* application. It will follow changes in the `PTP` port states, adjusting the synchronization between the NIC hardware clocks accordingly. The system clock is not synchronized, unless the [option]`-r` option is also specified. If you want the system clock to be eligible to become a time source, specify the [option]`-r` option twice.

After making changes to `/etc/sysconfig/phc2sys`, restart the [application]*phc2sys* service from the command line by issuing a command as `root`:

----
~]# systemctl restart phc2sys
----

Under normal circumstances, use [command]#systemctl# commands to start, stop, and restart the [application]*phc2sys* service.

When you do not want to start [application]*phc2sys* as a service, you can start it from the command line. For example, enter the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}phc2sys -a -r
----

The [option]`-a` option causes [application]*phc2sys* to read the clocks to be synchronized from the [application]*ptp4l* application. If you want the system clock to be eligible to become a time source, specify the [option]`-r` option twice.

Alternately, use the [option]`-s` option to synchronize the system clock to a specific interface's `PTP` hardware clock. For example:

[subs="attributes"]
----
~]#{nbsp}phc2sys -s em3 -w
----

The [option]`-w` option waits for the running [application]*ptp4l* application to synchronize the `PTP` clock and then retrieves the *TAI* to *UTC* offset from [application]*ptp4l*.

Normally, `PTP` operates in the _International Atomic Time_ (*TAI*) timescale, while the system clock is kept in _Coordinated Universal Time_ (*UTC*). The current offset between the TAI and UTC timescales is 35 seconds. The offset changes when leap seconds are inserted or deleted, which typically happens every few years. The [option]`-O` option needs to be used to set this offset manually when the [option]`-w` is not used, as follows:

[subs="attributes"]
----
~]#{nbsp}phc2sys -s em3 -O -35
----

Once the [application]*phc2sys* servo is in a locked state, the clock will not be stepped, unless the [option]`-S` option is used. This means that the [application]*phc2sys* program should be started after the [application]*ptp4l* program has synchronized the `PTP` hardware clock. However, with [option]`-w`, it is not necessary to start [application]*phc2sys* after [application]*ptp4l* as it will wait for it to synchronize the clock.

The [application]*phc2sys* program can also be started as a service by running:

[subs="attributes"]
----
~]#{nbsp}systemctl start phc2sys
----

When running as a service, options are specified in the `/etc/sysconfig/phc2sys` file. More information on the different [application]*phc2sys* options can be found in the `phc2sys(8)` man page.

Note that the examples in this section assume the command is run on a slave system or slave port.

[[sec-Verifying_Time_Synchronization]]
== Verifying Time Synchronization

When `PTP` time synchronization is working properly, new messages with offsets and frequency adjustments will be printed periodically to the [application]*ptp4l* and [application]*phc2sys* (if hardware time stamping is used) outputs. These values will eventually converge after a short period of time. These messages can be seen in `/var/log/messages` file. An example of the output follows:

----

ptp4l[352.359]: selected /dev/ptp0 as PTP clock
ptp4l[352.361]: port 1: INITIALIZING to LISTENING on INITIALIZE
ptp4l[352.361]: port 0: INITIALIZING to LISTENING on INITIALIZE
ptp4l[353.210]: port 1: new foreign master 00a069.fffe.0b552d-1
ptp4l[357.214]: selected best master clock 00a069.fffe.0b552d
ptp4l[357.214]: port 1: LISTENING to UNCALIBRATED on RS_SLAVE
ptp4l[359.224]: master offset       3304 s0 freq      +0 path delay      9202
ptp4l[360.224]: master offset       3708 s1 freq  -29492 path delay      9202
ptp4l[361.224]: master offset      -3145 s2 freq  -32637 path delay      9202
ptp4l[361.224]: port 1: UNCALIBRATED to SLAVE on MASTER_CLOCK_SELECTED
ptp4l[362.223]: master offset       -145 s2 freq  -30580 path delay      9202
ptp4l[363.223]: master offset       1043 s2 freq  -29436 path delay      8972
ptp4l[364.223]: master offset        266 s2 freq  -29900 path delay      9153
ptp4l[365.223]: master offset        430 s2 freq  -29656 path delay      9153
ptp4l[366.223]: master offset        615 s2 freq  -29342 path delay      9169
ptp4l[367.222]: master offset       -191 s2 freq  -29964 path delay      9169
ptp4l[368.223]: master offset        466 s2 freq  -29364 path delay      9170
ptp4l[369.235]: master offset         24 s2 freq  -29666 path delay      9196
ptp4l[370.235]: master offset       -375 s2 freq  -30058 path delay      9238
ptp4l[371.235]: master offset        285 s2 freq  -29511 path delay      9199
ptp4l[372.235]: master offset        -78 s2 freq  -29788 path delay      9204

----

An example of the [application]*phc2sys* output follows:

----

phc2sys[526.527]: Waiting for ptp4l...
phc2sys[527.528]: Waiting for ptp4l...
phc2sys[528.528]: phc offset     55341 s0 freq      +0 delay   2729
phc2sys[529.528]: phc offset     54658 s1 freq  -37690 delay   2725
phc2sys[530.528]: phc offset       888 s2 freq  -36802 delay   2756
phc2sys[531.528]: phc offset      1156 s2 freq  -36268 delay   2766
phc2sys[532.528]: phc offset       411 s2 freq  -36666 delay   2738
phc2sys[533.528]: phc offset       -73 s2 freq  -37026 delay   2764
phc2sys[534.528]: phc offset        39 s2 freq  -36936 delay   2746
phc2sys[535.529]: phc offset        95 s2 freq  -36869 delay   2733
phc2sys[536.529]: phc offset      -359 s2 freq  -37294 delay   2738
phc2sys[537.529]: phc offset      -257 s2 freq  -37300 delay   2753
phc2sys[538.529]: phc offset       119 s2 freq  -37001 delay   2745
phc2sys[539.529]: phc offset       288 s2 freq  -36796 delay   2766
phc2sys[540.529]: phc offset      -149 s2 freq  -37147 delay   2760
phc2sys[541.529]: phc offset      -352 s2 freq  -37395 delay   2771
phc2sys[542.529]: phc offset       166 s2 freq  -36982 delay   2748
phc2sys[543.529]: phc offset        50 s2 freq  -37048 delay   2756
phc2sys[544.530]: phc offset       -31 s2 freq  -37114 delay   2748
phc2sys[545.530]: phc offset      -333 s2 freq  -37426 delay   2747
phc2sys[546.530]: phc offset       194 s2 freq  -36999 delay   2749
----

For [application]*ptp4l* there is also a directive, [option]`summary_interval`, to reduce the output and print only statistics, as normally it will print a message every second or so. For example, to reduce the output to every `1024` seconds, add the following line to the `/etc/ptp4l.conf` file:

----
summary_interval 10
----

An example of the [application]*ptp4l* output, with [option]`summary_interval 6`, follows:

----

ptp4l: [615.253] selected /dev/ptp0 as PTP clock
ptp4l: [615.255] port 1: INITIALIZING to LISTENING on INITIALIZE
ptp4l: [615.255] port 0: INITIALIZING to LISTENING on INITIALIZE
ptp4l: [615.564] port 1: new foreign master 00a069.fffe.0b552d-1
ptp4l: [619.574] selected best master clock 00a069.fffe.0b552d
ptp4l: [619.574] port 1: LISTENING to UNCALIBRATED on RS_SLAVE
ptp4l: [623.573] port 1: UNCALIBRATED to SLAVE on MASTER_CLOCK_SELECTED
ptp4l: [684.649] rms  669 max 3691 freq -29383 ± 3735 delay  9232 ± 122
ptp4l: [748.724] rms  253 max  588 freq -29787 ± 221 delay  9219 ± 158
ptp4l: [812.793] rms  287 max  673 freq -29802 ± 248 delay  9211 ± 183
ptp4l: [876.853] rms  226 max  534 freq -29795 ± 197 delay  9221 ± 138
ptp4l: [940.925] rms  250 max  562 freq -29801 ± 218 delay  9199 ± 148
ptp4l: [1004.988] rms  226 max  525 freq -29802 ± 196 delay  9228 ± 143
ptp4l: [1069.065] rms  300 max  646 freq -29802 ± 259 delay  9214 ± 176
ptp4l: [1133.125] rms  226 max  505 freq -29792 ± 197 delay  9225 ± 159
ptp4l: [1197.185] rms  244 max  688 freq -29790 ± 211 delay  9201 ± 162

----

To reduce the output from the [application]*phc2sys*, it can be called it with the [option]`-u` option as follows:

[subs="attributes"]
----
~]#{nbsp}phc2sys -u summary-updates
----

Where _summary-updates_ is the number of clock updates to include in summary statistics. An example follows:

[subs="attributes"]
----

~]#{nbsp}phc2sys -s em3 -w -m -u 60
phc2sys[700.948]: rms 1837 max 10123 freq -36474 ± 4752 delay  2752 ±  16
phc2sys[760.954]: rms  194 max  457 freq -37084 ± 174 delay  2753 ±  12
phc2sys[820.963]: rms  211 max  487 freq -37085 ± 185 delay  2750 ±  19
phc2sys[880.968]: rms  183 max  440 freq -37102 ± 164 delay  2734 ±  91
phc2sys[940.973]: rms  244 max  584 freq -37095 ± 216 delay  2748 ±  16
phc2sys[1000.979]: rms  220 max  573 freq -36666 ± 182 delay  2747 ±  43
phc2sys[1060.984]: rms  266 max  675 freq -36759 ± 234 delay  2753 ±  17

----

[[sec-Serving_PTP_Time_with_NTP]]
== Serving PTP Time with NTP

The `ntpd` daemon can be configured to distribute the time from the system clock synchronized by [application]*ptp4l* or [application]*phc2sys* by using the LOCAL reference clock driver. To prevent `ntpd` from adjusting the system clock, the `ntp.conf` file must not specify any `NTP` servers. The following is a minimal example of `ntp.conf`:

[subs="attributes"]
----

~]#{nbsp}cat /etc/ntp.conf
server   127.127.1.0
fudge    127.127.1.0 stratum 0
----

[NOTE]
====

When the `DHCP` client program, [application]*dhclient*, receives a list of `NTP` servers from the `DHCP` server, it adds them to `ntp.conf` and restarts the service. To disable that feature, add [command]#PEERNTP=no# to `/etc/sysconfig/network`.

====

[[sec-Serving_NTP_Time_with_PTP]]
== Serving NTP Time with PTP

`NTP` to `PTP` synchronization in the opposite direction is also possible. When `ntpd` is used to synchronize the system clock, [application]*ptp4l* can be configured with the [option]`priority1` option (or other clock options included in the best master clock algorithm) to be the grandmaster clock and distribute the time from the system clock via `PTP`:

[subs="attributes"]
----
~]#{nbsp}cat /etc/ptp4l.conf
[global]
priority1 127
[em3]
# ptp4l -f /etc/ptp4l.conf
----

With hardware time stamping, [application]*phc2sys* needs to be used to synchronize the `PTP` hardware clock to the system clock.
If running [application]*phc2sys* as a service, edit the `/etc/sysconfig/phc2sys` configuration file. The default setting in the `/etc/sysconfig/phc2sys` file is as follows:

[subs="quotes"]
----
OPTIONS="-a -r"
----

As `root`, edit that line as follows:

----
~]# vi /etc/sysconfig/phc2sys
OPTIONS="-a -r -r"
----

The [option]`-r` option is used twice here to allow synchronization of the `PTP` hardware clock on the NIC from the system clock.
Restart the [application]*phc2sys* service for the changes to take effect:

----
~]# systemctl restart phc2sys
----

To prevent quick changes in the `PTP` clock's frequency, the synchronization to the system clock can be loosened by using smaller [option]`P` (proportional) and [option]`I` (integral) constants for the PI servo:

[subs="attributes"]
----
~]#{nbsp}phc2sys -a -r -r -P 0.01 -I 0.0001
----

[[sec-Synchronize_to_PTP_or_NTP_Time_Using_timemaster]]
== Synchronize to PTP or NTP Time Using timemaster

When there are multiple `PTP` domains available on the network, or fallback to `NTP` is needed, the [application]*timemaster* program can be used to synchronize the system clock to all available time sources. The `PTP` time is provided by [application]*phc2sys* and [application]*ptp4l* via _shared memory driver_ (*SHM* reference clocks to `chronyd` or `ntpd` (depending on the `NTP` daemon that has been configured on the system). The `NTP` daemon can then compare all time sources, both `PTP` and `NTP`, and use the best sources to synchronize the system clock.

On start, [application]*timemaster* reads a configuration file that specifies the `NTP` and `PTP` time sources, checks which network interfaces have their own or share a `PTP` hardware clock (PHC), generates configuration files for [application]*ptp4l* and `chronyd` or `ntpd`, and starts the [application]*ptp4l*, [application]*phc2sys*, and `chronyd` or `ntpd` processes as needed. It will remove the generated configuration files on exit. It writes configuration files for `chronyd`, `ntpd`, and [application]*ptp4l* to `/var/run/timemaster/`.

[[sec-Starting_timemaster_as_a_Service]]
=== Starting timemaster as a Service

To start [application]*timemaster* as a service, issue the following command as `root`:

[subs="attributes"]
----
~]#{nbsp}systemctl start timemaster
----

This will read the options in  `/etc/timemaster.conf`.

[[sec-Understanding_the_timemaster_Configuration_File]]
=== Understanding the timemaster Configuration File

{MAJOROS} provides a default `/etc/timemaster.conf` file with a number of sections containing default options. The section headings are enclosed in brackets.

To view the default configuration, issue a command as follows:

----
~]$ less /etc/timemaster.conf
# Configuration file for timemaster

#[ntp_server ntp-server.local]
#minpoll 4
#maxpoll 4

#[ptp_domain 0]
#interfaces eth0

[timemaster]
ntp_program chronyd

[chrony.conf]
include /etc/chrony.conf

[ntp.conf]
includefile /etc/ntp.conf

[ptp4l.conf]

[chronyd]
path /usr/sbin/chronyd
options -u chrony

[ntpd]
path /usr/sbin/ntpd
options -u ntp:ntp -g

[phc2sys]
path /usr/sbin/phc2sys

[ptp4l]
path /usr/sbin/ptp4l
----

Notice the section named as follows:

[subs="macros"]
----
[ntp_server pass:quotes[_address_]]
----

This is an example of an `NTP` server section, "`ntp-server.local`" is an example of a host name for an `NTP` server on the local LAN. Add more sections as required using a host name or `IP` address as part of the section name. Note that the short polling values in that example section are not suitable for a public server, see xref:servers/Configuring_NTP_Using_ntpd.adoc#ch-Configuring_NTP_Using_ntpd[Configuring NTP Using ntpd] for an explanation of suitable [option]`minpoll` and [option]`maxpoll` values.

Notice the section named as follows:

[subs="macros"]
----
[ptp_domain pass:quotes[_number_]]
----

A "`PTP domain`" is a group of one or more `PTP` clocks that synchronize to each other. They may or may not be synchronized to clocks in another domain. Clocks that are configured with the same domain number make up the domain. This includes a `PTP` grandmaster clock. The domain number in each "`PTP domain`" section needs to correspond to one of the `PTP` domains configured on the network.

An instance of [application]*ptp4l* is started for every interface which has its own `PTP` clock and hardware time stamping is enabled automatically. Interfaces that support hardware time stamping have a `PTP` clock (PHC) attached, however it is possible for a group of interfaces on a NIC to share a PHC. A separate [application]*ptp4l* instance will be started for each group of interfaces sharing the same PHC and for each interface that supports only software time stamping. All [application]*ptp4l* instances are configured to run as a slave. If an interface with hardware time stamping is specified in more than one `PTP` domain, then only the first [application]*ptp4l* instance created will have hardware time stamping enabled.

Notice the section named as follows:

[subs="quotes"]
----
[timemaster]
----

The default [application]*timemaster* configuration includes the system `ntpd` and chrony configuration (`/etc/ntp.conf` or `/etc/chronyd.conf`) in order to include the configuration of access restrictions and authentication keys. That means any `NTP` servers specified there will be used with [application]*timemaster* too.

The section headings are as follows:

* [option]`[ntp_server ntp-server.local]` — Specify polling intervals for this server. Create additional sections as required. Include the host name or `IP` address in the section heading.

* [option]`[ptp_domain 0]` — Specify interfaces that have `PTP` clocks configured for this domain. Create additional sections with, the appropriate domain number, as required.

* [option]`[timemaster]` — Specify the `NTP` daemon to be used. Possible values are `chronyd` and `ntpd`.

* [option]`[chrony.conf]` — Specify any additional settings to be copied to the configuration file generated for `chronyd`.

* [option]`[ntp.conf]` — Specify any additional settings to be copied to the configuration file generated for `ntpd`.

* [option]`[ptp4l.conf]` — Specify options to be copied to the configuration file generated for [application]*ptp4l*.

* [option]`[chronyd]` — Specify any additional settings to be passed on the command line to `chronyd`.

* [option]`[ntpd]` — Specify any additional settings to be passed on the command line to `ntpd`.

* [option]`[phc2sys]` — Specify any additional settings to be passed on the command line to [application]*phc2sys*.

* [option]`[ptp4l]` — Specify any additional settings to be passed on the command line to all instances of [application]*ptp4l*.

The section headings and there contents are explained in detail in the `timemaster(8)` manual page.

[[sec-Configuring_timemaster_Options]]
=== Configuring timemaster Options

[[proc-Editing_the_timemaster_Configuration_File]]
.Editing the timemaster Configuration File
. To change the default configuration, open the `/etc/timemaster.conf` file for editing as `root`:

----
~]# vi /etc/timemaster.conf
----

. For each `NTP` server you want to control using [application]*timemaster*, create `[ntp_server _address_pass:attributes[{blank}]]` sections . Note that the short polling values in the example section are not suitable for a public server, see xref:servers/Configuring_NTP_Using_ntpd.adoc#ch-Configuring_NTP_Using_ntpd[Configuring NTP Using ntpd] for an explanation of suitable [option]`minpoll` and [option]`maxpoll` values.

. To add interfaces that should be used in a domain, edit the `#[ptp_domain 0]` section and add the interfaces. Create additional domains as required. For example:

----
[ptp_domain 0]
       interfaces eth0

       [ptp_domain 1]
       interfaces eth1
----

. If required to use `ntpd` as the `NTP` daemon on this system, change the default entry in the `[timemaster]` section from `chronyd` to `ntpd`. See xref:servers/Configuring_NTP_Using_the_chrony_Suite.adoc#ch-Configuring_NTP_Using_the_chrony_Suite[Configuring NTP Using the chrony Suite] for information on the differences between ntpd and chronyd.

. If using `chronyd` as the `NTP` server on this system, add any additional options below the default [option]`include /etc/chrony.conf` entry in the `[chrony.conf]` section. Edit the default [option]`include` entry if the path to `/etc/chrony.conf` is known to have changed.

. If using `ntpd` as the `NTP` server on this system, add any additional options below the default [option]`include /etc/ntp.conf` entry in the `[ntp.conf]` section. Edit the default [option]`include` entry if the path to `/etc/ntp.conf` is known to have changed.

. In the `[ptp4l.conf]` section, add any options to be copied to the configuration file generated for [application]*ptp4l*. This chapter documents common options and more information is available in the `ptp4l(8)` manual page.

. In the `[chronyd]` section, add any command line options to be passed to `chronyd` when called by [application]*timemaster*. See xref:servers/Configuring_NTP_Using_the_chrony_Suite.adoc#ch-Configuring_NTP_Using_the_chrony_Suite[Configuring NTP Using the chrony Suite] for information on using `chronyd`.

. In the `[ntpd]` section, add any command line options to be passed to `ntpd` when called by [application]*timemaster*. See xref:servers/Configuring_NTP_Using_ntpd.adoc#ch-Configuring_NTP_Using_ntpd[Configuring NTP Using ntpd] for information on using `ntpd`.

. In the `[phc2sys]` section, add any command line options to be passed to [application]*phc2sys* when called by [application]*timemaster*. This chapter documents common options and more information is available in the `phy2sys(8)` manual page.

. In the `[ptp4l]` section, add any command line options to be passed to [application]*ptp4l* when called by [application]*timemaster*. This chapter documents common options and more information is available in the `ptp4l(8)` manual page.

. Save the configuration file and restart [application]*timemaster* by issuing the following command as `root`:

----
~]# systemctl restart timemaster
----

[[sec-Improving_Accuracy]]
== Improving Accuracy

Previously, test results indicated that disabling the tickless kernel capability could significantly improve the stability of the system clock, and thus improve the `PTP` synchronization accuracy (at the cost of increased power consumption). The kernel tickless mode can be disabled by adding [option]`nohz=off` to the kernel boot option parameters. However, recent improvements applied to `kernel-3.10.0-197.fc21` have greatly improved the stability of the system clock and the difference in stability of the clock with and without [option]`nohz=off` should be much smaller now for most users.

The [application]*ptp4l* and [application]*phc2sys* applications can be configured to use a new adaptive servo. The advantage over the PI servo is that it does not require configuration of the PI constants to perform well. To make use of this for [application]*ptp4l*, add the following line to the `/etc/ptp4l.conf` file:

----
clock_servo linreg
----

After making changes to `/etc/ptp4l.conf`, restart the [application]*ptp4l* service from the command line by issuing the following command as `root`:

----
~]# systemctl restart ptp4l
----

To make use of this for [application]*phc2sys*, add the following line to the `/etc/sysconfig/phc2sys` file:

[subs="quotes"]
----
-E linreg
----

After making changes to `/etc/sysconfig/phc2sys`, restart the [application]*phc2sys* service from the command line by issuing the following command as `root`:

----
~]# systemctl restart phc2sys
----

[[sec-PTP_additional_resources]]
== Additional Resources

The following sources of information provide additional resources regarding `PTP` and the [application]*ptp4l* tools.

[[sec-PTP-docs-inst]]
=== Installed Documentation

* `ptp4l(8)` man page — Describes [application]*ptp4l* options including the format of the configuration file.

* `pmc(8)` man page — Describes the `PTP` management client and its command options.

* `phc2sys(8)` man page — Describes a tool for synchronizing the system clock to a `PTP` hardware clock (PHC).

* `timemaster(8)` man page — Describes a program that uses [application]*ptp4l* and [application]*phc2sys* to synchronize the system clock using `chronyd` or `ntpd`.

[[sec-PTP_useful-websites]]
=== Useful Websites

link:++http://linuxptp.sourceforge.net/++[]:: The Linux PTP project.

link:++https://www.nist.gov/el/isd/ieee/ieee1588.cfm++[]:: The IEEE 1588 Standard.


:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Web_Servers]]
= Web Servers
indexterm:[HTTP server,Apache HTTP Server]indexterm:[web server,Apache HTTP Server]
A _web server_ is a network service that serves content to a client over the web. This typically means web pages, but any other documents can be served as well. Web servers are also known as HTTP servers, as they use the _hypertext transport protocol_ (*HTTP*).

include::{partialsdir}/servers/The_Apache_HTTP_Server.adoc[]
